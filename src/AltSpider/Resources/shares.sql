

--
-- Database structure for database 'share_index'
--


DROP DATABASE IF EXISTS `share_index`;
CREATE DATABASE IF NOT EXISTS `share_index` DEFAULT CHARACTER SET utf8 ;

USE `share_index`;

--
-- Table structure for table 'node'
--

CREATE TABLE IF NOT EXISTS `node` (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `parent_id` INT(10) UNSIGNED DEFAULT NULL,
    `lvl` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
    `name` VARCHAR(244) NOT NULL,
    `modified_on` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `created_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT `pk_node_id` PRIMARY KEY (`id`),
    UNIQUE KEY `ui_node_parent_id_name` (`parent_id`,`name`),
    CONSTRAINT `fk_node_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `node` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TRIGGER `tr_node_after_update` 
	BEFORE UPDATE ON `node` FOR EACH ROW
	SET NEW.modified_on = CURRENT_TIMESTAMP;

--
-- Dumping data for table 'node'
--

LOCK TABLES `node` WRITE;

INSERT INTO `node` (`id`, `parent_id`, `lvl`, `name`, `modified_on`, `created_on`) VALUES
	('1',NULL,0,'root',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

UNLOCK TABLES;


--
-- Table structure for table 'relations'
--

CREATE TABLE IF NOT EXISTS `relations` (
    `child_id` INT(10) UNSIGNED NOT NULL,
    `parent_id` INT(10) UNSIGNED NOT NULL,
	CONSTRAINT `pk_relations_child_parent` PRIMARY KEY (`child_id`, `parent_id`),
	CONSTRAINT `fk_relations_child_id` FOREIGN KEY (`child_id`) REFERENCES `node` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `fk_relations_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `node` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table 'file_info'
--
CREATE TABLE `file_info` (
  `node_id` INT(10) UNSIGNED NOT NULL,
  `file_size` BIGINT(20) UNSIGNED DEFAULT NULL,
  `ctime` TIMESTAMP,
  `mtime` TIMESTAMP,
  `atime` TIMESTAMP,
  PRIMARY KEY (`node_id`),
  UNIQUE KEY `ui_file_info` (`node_id`),
  CONSTRAINT `fk_file_info_node_id` FOREIGN KEY (`node_id`) REFERENCES `node` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Reset data

DELETE FROM `file_info`;
DELETE FROM `node` WHERE `name` != 'root';
DELETE FROM `relations`;

DROP PROCEDURE IF EXISTS `proc_add_node`;

DELIMITER //

CREATE PROCEDURE `proc_add_node` ( $name VARCHAR(244), $parent_id INT, OUT $child_node_id INT )
BEGIN
    SELECT lvl + 1 INTO @newlvl FROM node WHERE id = $parent_id;

    INSERT INTO node (name, parent_id, lvl, modified_on) 
        VALUES ($name, $parent_id, @newlvl, CURRENT_TIMESTAMP);

    SET $child_node_id = @@identity;        

    WHILE $parent_id IS NOT NULL DO
        INSERT INTO relations VALUES ($child_node_id, $parent_id);
        SELECT parent_id INTO $parent_id FROM node WHERE id = $parent_id;
    END WHILE;
END//

DELIMITER ;


DROP PROCEDURE IF EXISTS `proc_get_node`;

DELIMITER //

CREATE PROCEDURE `proc_get_node` ( $name VARCHAR(244), $parent_id INT, OUT $child_node_id INT )
BEGIN
    SELECT `id` INTO $child_node_id FROM `node` WHERE `parent_id` = $parent_id AND name = $name;
END//

DELIMITER ;


DROP PROCEDURE IF EXISTS `proc_save_node`;

DELIMITER //

CREATE PROCEDURE `proc_save_node` ( $name VARCHAR(244), $parent_id INT, OUT $child_node_id INT )
BEGIN
    CALL `proc_get_node` ( $name, $parent_id, $child_node_id );

    IF $child_node_id IS NULL THEN
        CALL `proc_add_node` ( $name, $parent_id, $child_node_id );
    END IF;
END//

DELIMITER ;


DROP FUNCTION IF EXISTS `get_path`;

DELIMITER //

CREATE FUNCTION `get_path` ( node_id INT )
RETURNS VARCHAR(266)
READS SQL DATA
BEGIN
    SELECT IF(id = 1, '/', CONCAT('/', name)) INTO @val FROM node WHERE id = node_id;

    SELECT IFNULL(CONCAT('/', GROUP_CONCAT(parent.name SEPARATOR '/'), @val), @val)
        INTO @val
        FROM relations
        INNER JOIN node AS parent ON relations.parent_id = parent.id
        WHERE relations.child_id = node_id AND parent.parent_id IS NOT NULL 
        ORDER BY parent.lvl DESC;

    RETURN @val;
END//

DELIMITER ;

DROP PROCEDURE IF EXISTS `proc_save_file_info`;

DELIMITER //

CREATE PROCEDURE `proc_save_file_info` ( $id INT, $size BIGINT, $ctime TIMESTAMP, $mtime TIMESTAMP, $atime TIMESTAMP)
BEGIN
    INSERT INTO `file_info` ( `node_id`, `file_size`, `ctime`, `mtime`, `atime` )
        VALUES ( $id, $size, $ctime, $mtime, $atime )
        ON DUPLICATE KEY UPDATE file_size = $size, ctime = $ctime, mtime = $mtime, atime = $atime;
END//

DELIMITER ;

DROP VIEW IF EXISTS `view_file_system`;

CREATE VIEW `view_file_system` AS
SELECT `id`, `name`, get_path(id) as path
, `file_size` AS size, `ctime`, `mtime`, `atime`
, (SELECT COUNT(42) FROM relations WHERE parent_id = id) AS children
, (SELECT COUNT(42) FROM relations WHERE child_id = id) AS parents
FROM node LEFT JOIN file_info ON file_info.node_id = node.id
HAVING children = 0;

DROP VIEW IF EXISTS `view_shares`;

CREATE VIEW `view_shares` AS 
SELECT id, get_path(id) AS path, modified_on
FROM node WHERE lvl = 2;

