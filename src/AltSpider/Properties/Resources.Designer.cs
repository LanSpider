﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.1434
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AltSpider.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("AltSpider.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 
        ///
        ///--
        ///-- Database structure for database &apos;share_index&apos;
        ///--
        ///
        ///
        ///DROP DATABASE IF EXISTS `share_index`;
        ///CREATE DATABASE IF NOT EXISTS `share_index` DEFAULT CHARACTER SET utf8 ;
        ///
        ///USE `share_index`;
        ///
        ///--
        ///-- Table structure for table &apos;node&apos;
        ///--
        ///
        ///CREATE TABLE IF NOT EXISTS `node` (
        ///    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        ///    `parent_id` INT(10) UNSIGNED DEFAULT NULL,
        ///    `lvl` TINYINT(3) UNSIGNED NOT NULL DEFAULT &apos;0&apos;,
        ///    `name` VARCHAR(244) NOT NULL,
        ///    `modified_on` TIMESTAMP NOT NULL [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string shares {
            get {
                return ResourceManager.GetString("shares", resourceCulture);
            }
        }
    }
}
