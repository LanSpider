﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using ShareDiscoveryService;

namespace AltSpider
{
    public class AltSpider
    {
        public static DiscoveryTreeNode IndexFolder( string directoryName )
        {
            FileInfo dirinfo = new FileInfo( directoryName );

            DiscoveryTreeNode dir
                = new DiscoveryTreeNode
                      {
                          Path = directoryName,
                          Name = Path.GetFileName( directoryName ),
                          CreatedAt = dirinfo.CreationTime,
                          LastAccessedAt = dirinfo.LastAccessTime,
                          LastModifiedAt = dirinfo.LastWriteTime
                      };

            List<DiscoveryTreeNode> listing = new List<DiscoveryTreeNode>();

            try
            {
                string[] subdirs = Directory.GetDirectories( directoryName );
                string[] subfiles = Directory.GetFiles( directoryName );

                if ( subdirs.Length == 0 && subfiles.Length == 0 )
                {
                    return null;
                }

                dir.ChildNodes
                    = (
                          (
                              from directory in subdirs
                              let child = IndexFolder( directory )
                              where child != null
                              select child
                          ).Union(
                              from fileName in subfiles
                              let info = new FileInfo( fileName )
                              select
                                  new DiscoveryTreeNode
                                      {
                                          Path = fileName,
                                          Name = info.Name,
                                          Size = info.Length,
                                          CreatedAt = info.CreationTime,
                                          LastAccessedAt = info.LastAccessTime,
                                          LastModifiedAt = info.LastWriteTime
                                      }
                              )
                      ).ToList();
            }
            catch ( UnauthorizedAccessException ex )
            {
                Console.Error.WriteLine( "Access denied to {0}", directoryName );
            }
            catch ( IOException ex )
            {
                Console.Error.WriteLine( "Cannot read {0}: {1}", directoryName, ex.Message );
            }

            return dir;
        }
    }
}