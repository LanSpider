﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using MySql.Data.MySqlClient;

namespace AltSpider
{
    public class AltModel : IDisposable
    {
        private readonly string _connectionString;
        private readonly DbConnection _connection;

        public AltModel()
        {
            DbConnectionStringBuilder dbcsb
                = new DbConnectionStringBuilder
                      {
                          { "Data Source", "localhost" },
                          { "User ID", "root" },
                          { "Database", "share_index" },
                          { "CharSet", "utf8" }
                      };

            _connectionString = dbcsb.ConnectionString;

            _connection = new MySqlConnection( _connectionString );
            _connection.Open();
        }

        /// <summary>
        /// Saves given path to the database.
        /// </summary>
        /// <param name="path"></param>
        /// <returns>Leaf node ID</returns>
        /// <exception cref="ApplicationException">Thrown if saving failed.</exception>
        public int SavePath( string path )
        {
            IEnumerable<string> getTokens = GetTokens( path );

            int parentId = 1;

            foreach ( string token in getTokens )
            {
                parentId = SaveNode( token, parentId );
                if ( parentId == -1 )
                {
                    throw new ApplicationException( String.Format( "Saving node {0} of {1} failed.", token, path ) );
                }
            }

            return parentId;
        }

        /// <summary>
        /// Adds new node to the if does not exist in the database otherwise, loads the node.
        /// </summary>
        /// <param name="nodeName"></param>
        /// <param name="parentNodeId"></param>
        /// <returns>Child node ID</returns>
        private int SaveNode( string nodeName, int parentNodeId )
        {
            DbCommand cmd = _connection.CreateCommand();
            cmd.CommandText = "proc_save_node";
            cmd.CommandType = CommandType.StoredProcedure;

            DbParameter nameParam = cmd.CreateParameter();
            nameParam.Direction = ParameterDirection.Input;
            nameParam.ParameterName = "$name";
            nameParam.Value = nodeName;

            DbParameter parendIdParam = cmd.CreateParameter();
            parendIdParam.Direction = ParameterDirection.Input;
            parendIdParam.ParameterName = "$parent_id";
            parendIdParam.Value = parentNodeId;

            DbParameter childIdParam = cmd.CreateParameter();
            childIdParam.Direction = ParameterDirection.Output;
            childIdParam.ParameterName = "$child_node_id";

            cmd.Parameters.AddRange( new[] { nameParam, parendIdParam, childIdParam } );

            cmd.ExecuteNonQuery();

            int childNodeId = -1;
            if ( childIdParam.Value != null && childIdParam.Value != DBNull.Value )
            {
                childNodeId = (int) childIdParam.Value;
            }

            cmd.Dispose();

            return childNodeId;
        }

        /// <summary>
        /// Split given path into separate tokens (directory and file names).
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static IEnumerable<string> GetTokens( string path )
        {
            string[] patrs = path.Substring( 1 ).Split( new[] { '/', '\\' }, StringSplitOptions.RemoveEmptyEntries );
            patrs[ 0 ] = path[ 0 ] + patrs[ 0 ];
            return patrs;
        }

        public void Dispose()
        {
            if ( _connection.State != ConnectionState.Closed )
            {
                try
                {
                    _connection.Close();
                }
                catch ( DbException ex )
                {
                    Trace.WriteLine( ex.Message, "Closing connection" );
                }
            }
        }

        /// <summary>
        /// Save file info into the database.
        /// </summary>
        /// <param name="nodeId">File node id</param>
        /// <param name="size">File size in bytes</param>
        /// <param name="ctime">File creation time</param>
        /// <param name="mtime">File's last modification time</param>
        /// <param name="atime">File's last access time</param>
        public void SaveFileInfo( int nodeId, long size, DateTime ctime, DateTime mtime, DateTime atime )
        {
            DbCommand cmd = _connection.CreateCommand();
            cmd.CommandText = "proc_save_file_info";
            cmd.CommandType = CommandType.StoredProcedure;

            DbParameter idParam = cmd.CreateParameter();
            idParam.Direction = ParameterDirection.Input;
            idParam.ParameterName = "$id";
            idParam.Value = nodeId;

            DbParameter sizeParam = cmd.CreateParameter();
            sizeParam.Direction = ParameterDirection.Input;
            sizeParam.ParameterName = "$size";
            sizeParam.Value = size;

            DbParameter ctimeParam = cmd.CreateParameter();
            ctimeParam.Direction = ParameterDirection.Input;
            ctimeParam.ParameterName = "$ctime";
            ctimeParam.Value = ctime;

            DbParameter mtimeParam = cmd.CreateParameter();
            mtimeParam.Direction = ParameterDirection.Input;
            mtimeParam.ParameterName = "$mtime";
            mtimeParam.Value = mtime;

            DbParameter atimeParam = cmd.CreateParameter();
            atimeParam.Direction = ParameterDirection.Input;
            atimeParam.ParameterName = "$atime";
            atimeParam.Value = atime;

            cmd.Parameters.AddRange( new[] { idParam, sizeParam, ctimeParam, mtimeParam, atimeParam } );

            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }

        /// <summary>
        /// Get list of shares stored in the database.
        /// </summary>
        /// <returns>List of shares</returns>
        public List<string> GetShares()
        {
            List<string> shares = new List<string>();

            DbCommand cmd = _connection.CreateCommand();
            cmd.CommandText = "SELECT path FROM view_shares";
            cmd.CommandType = CommandType.Text;

            DbDataReader reader = cmd.ExecuteReader();
            if ( reader.HasRows )
            {
                while ( reader.Read() )
                {
                    shares.Add( reader[ "path" ] as string );
                }
            }
            reader.Close();
            cmd.Dispose();

            return shares;
        }
    }
}