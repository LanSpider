﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel.Channels;
using System.Threading;

using Indexer.ShareDiscoveryReference;

using ShareDiscovery;

namespace Indexer
{
    internal class Program
    {
        private static int _sleepTimeout = /*1 minute*/ 60000;
        private static int _numSharesIndexed = 0;
        private static int _nDenial = 0;

        private static void Main( string[] args )
        {
            ShareDiscoveryServiceClient client = new ShareDiscoveryServiceClient();
            while ( true )
            {
                string share = null;
                Stopwatch startWatch = new Stopwatch();
                startWatch.Start();

                string errorMessage = "Got no shares to index.";
                if ( _numSharesIndexed == 0 )
                {
                    errorMessage += " Share discovery service is probably initializing now.";
                }

                try
                {
                    share = (new StreamReader(client.GetShareToIndex())).ReadToEnd();
                }
                catch ( TimeoutException )
                {
                    errorMessage = "Share discovery service timed out.";
                }

                if ( String.IsNullOrEmpty( share ) )
                {
                    ++_nDenial;
                    double timeout = _sleepTimeout * Math.Sqrt( _nDenial );
                    Console.WriteLine( "[{0}] Error: {1} Sleeping for {2:0.##} seconds.", DateTime.Now, errorMessage,
                                       timeout / 1000f );
                    Thread.Sleep( (int) timeout );
                    continue;
                }
                _nDenial = 0;

                Console.WriteLine( "[{0}] About to index share {1}.", DateTime.Now, share );

                try
                {
                    Stopwatch indexWatch = new Stopwatch();

                    indexWatch.Start();
                    TreeNode shareNode = IndexFolder( share );
                    indexWatch.Stop();

                    if ( shareNode != null )
                    {
                        if ( startWatch.IsRunning )
                        {
                            startWatch.Stop();
                            Console.WriteLine( "[{0}] Share discovery service initialized in {1}", DateTime.Now,
                                               startWatch.Elapsed );
                        }

                        string[] parts = share.Split( new[] { '\\' }, StringSplitOptions.RemoveEmptyEntries );

                        FileInfo shareInfo = new FileInfo( share );
                        shareNode.CreationTime = shareInfo.CreationTime;
                        shareNode.AccessTime = shareInfo.LastAccessTime;
                        shareNode.ModificationTime = shareInfo.LastWriteTime;

                        TreeNode compNode = new TreeNode();
                        compNode.Name = "//" + parts[ 0 ];
                        compNode.Children = new[] { shareNode };

                        // Count indexed nodes.
                        uint numNodes = 0;
                        Queue<TreeNode> queue = new Queue<TreeNode>();
                        queue.Enqueue( compNode );

                        while ( queue.Count > 0 )
                        {
                            TreeNode next = queue.Dequeue();
                            ++numNodes;
                            if ( next.Children != null )
                            {
                                foreach ( TreeNode child in next.Children )
                                {
                                    queue.Enqueue( child );
                                }
                            }
                        }

                        if ( numNodes > 3 )
                        {
                            ++_numSharesIndexed;
                            Console.WriteLine( "[{0}] Indexed {1}-th share {2} in {3}. {4} nodes in graph.",
                                               DateTime.Now,
                                               _numSharesIndexed,
                                               share, indexWatch.Elapsed, numNodes );

                            Stream stream = new MemoryStream();
                            IFormatter fmt = new BinaryFormatter();
                            fmt.Serialize( stream, compNode );
                            stream.Seek( 0, SeekOrigin.Begin );
                            client.SaveIndex( stream );
                            stream.Close();

                            Console.WriteLine( "[{0}] Saved share {1}.", DateTime.Now, share );
                        }
                    }
                    else
                    {
                        Console.WriteLine( "[{0}] Wasted {1} on indexing {2}.", DateTime.Now, indexWatch.Elapsed, share );
                    }
                }
                catch ( IOException ex )
                {
                    Console.Error.WriteLine( "[{0}] Error: failed to save {1}: {2}", DateTime.Now, share, ex.Message );
                }
            }
        }

        public static TreeNode IndexFolder( string directoryName )
        {
            FileInfo dirinfo = null;
            TreeNode dir = null;

            try
            {
                dirinfo = new FileInfo( directoryName );

                dir = new TreeNode
                          {
                              Name = Path.GetFileName( directoryName ),
                              CreationTime = dirinfo.CreationTime,
                              AccessTime = dirinfo.LastAccessTime,
                              ModificationTime = dirinfo.LastWriteTime
                          };

                List<TreeNode> listing = new List<TreeNode>();

                try
                {
                    string[] subdirs = Directory.GetDirectories( directoryName );
                    string[] subfiles = Directory.GetFiles( directoryName );

                    if ( subdirs.Length == 0 && subfiles.Length == 0 )
                    {
                        return null;
                    }

                    dir.Children
                        = (
                              (
                                  from directory in subdirs
                                  let child = IndexFolder( directory )
                                  where child != null && child.Name != ".svn"
                                  select child
                              ).Union(
                                  from fileName in subfiles
                                  let info = new FileInfo( fileName )
                                  select
                                      new TreeNode
                                          {
                                              Name = info.Name,
                                              Size = info.Length,
                                              CreationTime = info.CreationTime,
                                              AccessTime = info.LastAccessTime,
                                              ModificationTime = info.LastWriteTime
                                          }
                                  )
                          ).ToList();
                }
                catch ( UnauthorizedAccessException ex )
                {
                    Console.Error.WriteLine( "Access denied to {0}", directoryName );
                }
                catch ( IOException ex )
                {
                    Console.Error.WriteLine( "Cannot read {0}: {1}", directoryName, ex.Message );
                }
            }
            catch ( IOException ex )
            {
                Console.Error.WriteLine( "Path {0} not found.", directoryName );
            }

            return dir;
        }
    }
}