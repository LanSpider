﻿using System;

namespace Extensions
{
    public static class Extensions
    {
        /// <summary>
        /// Get the array slice between the index (inclusive and end of array).
        /// </summary>
        public static T[] Slice< T >( this T[] source, int start )
        {
            // Return new array
            T[] res = new T[source.Length - start];
            for ( int i = 0; i < source.Length - start; ++i )
            {
                res[ i ] = source[ i + start ];
            }
            return res;
        }

        /// <summary>
        /// Get the array slice between the two indexes.
        /// Inclusive for start index, exclusive for end index.
        /// </summary>
        public static T[] Slice< T >( this T[] source, int start, int end )
        {
            // Handles negative ends
            if ( end < 0 )
            {
                end = source.Length - start - end - 1;
            }
            int len = end - start;

            // Return new array
            T[] res = new T[len];
            for ( int i = 0; i < len; ++i )
            {
                res[ i ] = source[ i + start ];
            }
            return res;
        }

        /// <summary>
        /// Get count of arguments ocurances in this string.
        /// </summary>
        /// <param name="string"></param>
        /// <param name="char"></param>
        /// <returns></returns>
        public static int Count( this string @string, char @char )
        {
            int count = 0;
            int index = 0;
            while ( index != -1 )
            {
                index = @string.IndexOf( @char, index );
                if ( index != -1 )
                {
                    ++count;
                    ++index;
                }
            }

            return count;
        }

        /// <summary>
        /// Replaces this string with the text equivalent of the value of 
        /// specified System.Object instance.
        /// </summary>
        /// <param name="string"></param>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static string Fmt( this string @string, object arg )
        {
            return String.Format( @string, arg );
        }

        /// <summary>
        /// Replaces this string with the text equivalent of the value of 
        /// two specified System.Object instances.
        /// </summary>
        /// <param name="string"></param>
        /// <param name="arg0"></param>
        /// <param name="arg1"></param>
        /// <returns></returns>
        public static string Fmt( this string @string, object arg0, object arg1 )
        {
            return String.Format( @string, arg0, arg1 );
        }

        /// <summary>
        /// Replaces this string with the text equivalent of the value of 
        /// three specified System.Object instances.
        /// </summary>
        /// <param name="string"></param>
        /// <param name="arg0"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <returns></returns>
        public static string Fmt( this string @string, object arg0, object arg1, object arg2 )
        {
            return String.Format( @string, arg0, arg1, arg2 );
        }


        /// <summary>
        /// eplaces this string with the text equivalent of the value of 
        /// a corresponding System.Object instance in a specified array.
        /// </summary>
        /// <param name="string"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Fmt( this string @string, params object[] args )
        {
            return String.Format( @string, args );
        }
    }
}