﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace ShareDiscovery
{
    // NOTE: If you change the class name "ShareDiscoveryService" here, you must also update the reference to "ShareDiscoveryService" in App.config.
    public class ShareDiscoveryService : IShareDiscoveryService
    {
        #region Implementation of IShareDiscoveryService

        /// <summary>
        /// <para>
        /// Return next share to be indexed. Member of <see cref="IShareDiscoveryService"/>.
        /// </para>
        /// <para>
        /// First (several) run(s) may return null as initialization may tame some time.
        /// </para>
        /// </summary>
        /// <returns>Next share to index or NULL if no shares know at a time.</returns>
        public Stream GetShareToIndex()
        {
            string rval = IndexManager.Instance.GetNextShare();
            byte[] bytes = Encoding.UTF8.GetBytes( rval );
            return new MemoryStream( bytes );
        }


        /// <summary>
        /// Save index for a share previousely served. Member of <see cref="IShareDiscoveryService"/>.
        /// </summary>
        /// <param name="shareStream"></param>
        public void SaveIndex( Stream shareStream )
        {
            IFormatter formatter = new BinaryFormatter();
            TreeNode share = formatter.Deserialize( shareStream ) as TreeNode;

            if ( share != null )
            {
                EventLog.WriteEntry( "lanspider", "Have got " + share.Name + " to index." );
                IndexManager.Instance.SaveShare( share );
            }
        }

        #endregion
    }

    /// <summary>
    /// Stores information about path to be saved to the database.
    /// </summary>
    internal class PathInfo
    {
        private readonly string _path;
        private readonly long _size;

        private readonly DateTime _creationTime;
        private readonly DateTime _modificationTime;
        private readonly DateTime _accessTime;

        /// <summary>
        /// Initializes an instance of <see cref="PathInfo"/>.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="size"></param>
        /// <param name="creationTime"></param>
        /// <param name="modificationTime"></param>
        /// <param name="accessTime"></param>
        public PathInfo( string path, long size, DateTime creationTime, DateTime modificationTime, DateTime accessTime )
        {
            _path = path;
            _accessTime = accessTime;
            _modificationTime = modificationTime;
            _creationTime = creationTime;
            _size = size;
        }

        /// <summary>
        /// Initializes an instance of <see cref="PathInfo"/> based on an instance of 
        /// <see cref="TreeNode"/>.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="node"></param>
        public PathInfo( string path, TreeNode node )
        {
            _path = path;
            _accessTime = node.AccessTime;
            _modificationTime = node.ModificationTime;
            _creationTime = node.CreationTime;
            _size = node.Size;
        }

        /// <summary>
        /// Gets full file path.
        /// </summary>
        public string Path
        {
            get
            {
                return _path;
            }
        }

        /// <summary>
        /// Gets file size in bytes.
        /// </summary>
        public long Size
        {
            get
            {
                return _size;
            }
        }

        /// <summary>
        /// Gets file's time of creation.
        /// </summary>
        public DateTime CreationTime
        {
            get
            {
                return _creationTime;
            }
        }

        /// <summary>
        /// Gets file's last time of modification.
        /// </summary>
        public DateTime ModificationTime
        {
            get
            {
                return _modificationTime;
            }
        }

        /// <summary>
        /// Gets file's last access time.
        /// </summary>
        public DateTime AccessTime
        {
            get
            {
                return _accessTime;
            }
        }
    }
}