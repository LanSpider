﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace ShareDiscovery
{
    // NOTE: If you change the interface name "IShareDiscoveryService" here, you must also update the reference to "IShareDiscoveryService" in App.config.
    [ServiceContract]
    public interface IShareDiscoveryService
    {
        [OperationContract]
        Stream GetShareToIndex();

        [OperationContract]
        void SaveIndex( Stream shareStream );
    }

    [Serializable]
    [DataContract]
    public class TreeNode
    {
        [DataMember] public string Name;

        [DataMember] public IEnumerable<TreeNode> Children;

        [DataMember] public DateTime CreationTime;

        [DataMember] public DateTime AccessTime;

        [DataMember] public DateTime ModificationTime;

        [DataMember] public long Size = -1;
    }
}