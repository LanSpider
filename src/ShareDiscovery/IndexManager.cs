﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;

using AltSpider;

using LanSpider;

namespace ShareDiscovery
{
    internal class IndexManager
    {
        #region Singleton part

        private static IndexManager _instance;

        protected IndexManager()
        {
        }

        public static IndexManager Instance
        {
            get
            {
                if ( _instance == null )
                {
                    _instance = new IndexManager();
                }
                return _instance;
            }
        }

        #endregion

        private static Queue<string> _sharesQueue;
        private readonly object _queueLock = new object();
        private bool _workerLaunched;
        private static AltModel _model;
        private readonly object _modelLock = new object();

        private static Queue<TreeNode> _saveQueue;
        private readonly object _saveLock = new object();
        private bool _saveUnderway;

        /// <summary>
        /// Gets lazy initialized database model.
        /// </summary>
        private static AltModel Model
        {
            get
            {
                if ( _model == null )
                {
                    _model = new AltModel();
                }

                return _model;
            }
        }

        /// <summary>
        /// Gets A query of shares to index.
        /// </summary>
        private static Queue<string> SharesQueue
        {
            get
            {
                if ( _sharesQueue == null )
                {
                    _sharesQueue = new Queue<string>();
                }

                return _sharesQueue;
            }
        }

        /// <summary>
        /// Gets save queue.
        /// </summary>
        private static Queue<TreeNode> SaveQueue
        {
            get
            {
                if ( _saveQueue == null )
                {
                    _saveQueue = new Queue<TreeNode>();
                }

                return _saveQueue;
            }
        }


        public string GetNextShare()
        {
            string rval = null;
            bool bufferFull = false;

            lock(_saveLock)
            {
                bufferFull = SaveQueue.Count > 25000;
            }

            if ( !bufferFull )
            {
                bool queueReady = false;

                lock ( _queueLock )
                {
                    if ( SharesQueue != null && SharesQueue.Count > 0 )
                    {
                        rval = SharesQueue.Dequeue();
                    }

                    queueReady = SharesQueue != null && !String.IsNullOrEmpty( rval );
                }

                if ( !queueReady && !_workerLaunched )
                {
                    BackgroundWorker worker = new BackgroundWorker();
                    worker.DoWork += GetAllShares;
                    worker.RunWorkerAsync();

                    _workerLaunched = true;
                }
            }

            if ( rval == null )
            {
                rval = "";
            }

            return rval;
        }

        /// <summary>
        /// Load all network shares accessible to current computer. Method is run
        /// on it's own thread on service initialization. Results are saved added
        /// to <see cref="_sharesQueue"/>.
        /// </summary>
        /// <param name="sender"><see cref="BackgroundWorker"/> argument (not used)</param>
        /// <param name="e"><see cref="BackgroundWorker"/> argument (not used)</param>
        private void GetAllShares( object sender, DoWorkEventArgs e )
        {
            IEnumerable<string> known = Model.GetShares();

            string source = "LanSpider";
            string logName = "Application";

            IEnumerable<string> comps = NetworkBrowser.Instance.GetNetworkComputers();

            lock ( _queueLock )
            {
                _sharesQueue = new Queue<string>();
            }

            foreach ( string comp in comps )
            {
                IEnumerable<string> shares = NetworkBrowser.Instance.GetShares( comp );
                if ( shares != null )
                {
                    lock ( _queueLock )
                    {
                        foreach ( string share in shares )
                        {
                            if ( !known.Contains( share.Replace( '\\', '/' ) ) )
                            {
                                SharesQueue.Enqueue( share );
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Assynchroneous index saving method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveIndexAssync( object sender, DoWorkEventArgs e )
        {
            TreeNode share = null;

            lock ( _saveLock )
            {
                if ( SaveQueue.Count > 0 )
                {
                    share = SaveQueue.Dequeue();
                }
            }

            while ( share != null )
            {
                List<PathInfo> paths = RectifyPaths( share, "" ).ToList();
                EventLog.WriteEntry( "lanspider", "SaveIndexAssync waiting for the lock" );
                lock ( _modelLock )
                {
                    EventLog.WriteEntry( "lanspider", "saving info on " + paths[ 0 ].Path );
                    foreach ( PathInfo pathInfo in paths )
                    {
                        int nodeId = Model.SavePath( pathInfo.Path );

                        Model.SaveFileInfo( nodeId, pathInfo.Size, pathInfo.CreationTime, pathInfo.ModificationTime,
                                            pathInfo.AccessTime );
                    }
                }

                share = null;

                lock ( _saveLock )
                {
                    if ( SaveQueue.Count > 0 )
                    {
                        share = SaveQueue.Dequeue();
                    }
                    else
                    {
                        _saveUnderway = false;
                    }
                }
            }
        }

        /// <summary>
        /// Convert tree objects into string paths.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">Thrown if any of arguments is null.</exception>
        private static IEnumerable<PathInfo> RectifyPaths( TreeNode node, string parent )
        {
            if ( node == null )
            {
                throw new ArgumentNullException( "node" );
            }

            if ( parent == null )
            {
                throw new ArgumentNullException( "parent" );
            }

            IEnumerable<PathInfo> nodes;
            string path = Path.Combine( parent, node.Name );

            if ( node.Children == null )
            {
                nodes = new[] { new PathInfo( path, node ) };
            }
            else
            {
                nodes = from child in node.Children
                        where child != null
                        from childpath in RectifyPaths( child, path )
                        select childpath;
            }

            return nodes;
        }

        /// <summary>
        /// Save share information into the database.
        /// </summary>
        /// <param name="share"></param>
        internal void SaveShare( TreeNode share )
        {
            if ( share == null )
            {
                throw new ArgumentNullException( "share" );
            }

            EventLog.WriteEntry( "lanspider", "Have to save " + share.Name );

            lock ( _saveLock )
            {
                SaveQueue.Enqueue( share );

                if ( !_saveUnderway )
                {
                    EventLog.WriteEntry( "lanspider",
                                         "SaveShare starting a backgound worker for " + SaveQueue.Count + " items." );
                    BackgroundWorker worker = new BackgroundWorker();
                    worker.DoWork += SaveIndexAssync;
                    worker.RunWorkerAsync();
                    _saveUnderway = true;
                }
            }
        }
    }
}