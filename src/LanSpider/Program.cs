﻿using System;

namespace LanSpider
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            using (LanSpider spider = new LanSpider(NetworkBrowser.Instance))
            {
                try
                {
                    Console.WriteLine("Found {0} computers in network.", spider.NumComputers);
                    Console.WriteLine("{0} shares known.", spider.NumKnownShares);
                    Console.WriteLine("Have {0} shares to index.", spider.NumAvailableShares);

                    spider.IndexUnknownShares();
                    spider.IndexKnownAvailableShares();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("{0}", ex);
                    Console.ReadKey();
                }
            }
        }
    }
}