﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;

using Extensions;

using LanSpider.Database;
using LanSpider.Multithreading;

namespace LanSpider
{
    /// <summary>
    /// Gathers information on open shares in local area network.
    /// </summary>
    public class LanSpider : IDisposable
    {
        private readonly INetworkBrowser _browser;
        private List<string> _computers;
        private List<string> _knownShares;
        private List<string> _unknownShares;
        private List<string> _availableShares;
        private List<string> _knownAvailableShares;
        private readonly Manager _dbman;

        public LanSpider( INetworkBrowser browser )
        {
            _browser = browser;
            _dbman = new Manager();
        }

        #region Private functions

        private List<string> DiscoverNetworkComputers()
        {
            IEnumerable<string> x = _browser.GetNetworkComputers();
            List<string> result = new List<string>( x );
            return result;
        }

        #endregion Private functions

        #region Methods

        private List<string> GetAvailableShares()
        {
            Consumer<IEnumerable<string>> consumer = new Consumer<IEnumerable<string>>();

            Console.Write( "Indexing network computers ..." );

            DateTime start = DateTime.Now;
            WorkManager<string, IEnumerable<string>> manager
                = new WorkManager<string, IEnumerable<string>>( _browser.GetShares, consumer.Consume );

            manager.WorkProgressChanged += OnShareDiscoveryProgressChanged;

            manager.RunSync( Computers );
            TimeSpan duration = DateTime.Now - start;

            Console.WriteLine( "\rIndexing network computers done in {0}:{1}.", (int) duration.TotalMinutes,
                               duration.Seconds );

            /*IEnumerable<string> unknownShares
                = from bunch in consumer.Results
                  from share in bunch
                  where !KnownShares.Contains(share)
                  select share;*/
            Console.WriteLine( consumer.Results.Count() );
            IEnumerable<string> availableShares
                = from bunch in consumer.Results
                  from share in bunch
                  select share;
            return new List<string>( availableShares );
        }

        private List<string> GetUnknownShares()
        {
            return ( from share in _availableShares where !KnownShares.Contains( share ) select share ).ToList();
        }

        private List<string> GetKnownAvailableShares()
        {
            return ( from share in _availableShares where KnownShares.Contains( share ) select share ).ToList();
        }

        #endregion Methods

        #region Private properties

        private List<string> Computers
        {
            get
            {
                if ( _computers == null )
                {
                    _computers = DiscoverNetworkComputers();
                }

                return _computers;
            }
        }

        private List<string> KnownShares
        {
            get
            {
                if ( _knownShares == null )
                {
                    _knownShares = _dbman.GetRootFolders();
                }

                return _knownShares;
            }
        }

        private List<string> UnknownShares
        {
            get
            {
                if ( _unknownShares == null )
                {
                    _unknownShares = GetUnknownShares();
                }

                return _unknownShares;
            }
        }

        private List<string> KnownAvailableShares
        {
            get
            {
                if ( _knownAvailableShares == null )
                {
                    _knownAvailableShares = GetKnownAvailableShares();
                }
                return _knownAvailableShares;
            }
        }

        private List<string> AvailableShares
        {
            get
            {
                if ( _availableShares == null )
                {
                    _availableShares = GetAvailableShares();
                }

                return _availableShares;
            }
        }

        #endregion Private properties

        #region Public properties

        /// <summary>
        /// Gets the number of known computers.
        /// </summary>
        public int NumComputers
        {
            get
            {
                return Computers.Count;
            }
        }

        /// <summary>
        /// Gets the number of known shares (those share which were indexed).
        /// </summary>
        public int NumKnownShares
        {
            get
            {
                return KnownShares.Count;
            }
        }

        /// <summary>
        /// Gets the number of unknown shares (those shares to index).
        /// </summary>
        public int NumAvailableShares
        {
            get
            {
                return AvailableShares.Count;
            }
        }

        #endregion Public properties

        #region Implementation of IDisposable

        public void Dispose()
        {
            _dbman.Dispose();
        }

        #endregion

        private void IndexShares( IEnumerable<string> shares )
        {
            WorkManager<string, IEnumerable<FileInfo>> manager
                = new WorkManager<string, IEnumerable<FileInfo>>( IndexFolder, SaveShareIndex );

            manager.WorkProgressChanged += OnShareIndexProgressChanged;

            manager.RunSync( shares );
        }

        public void IndexUnknownShares()
        {
            IndexShares( UnknownShares );
        }

        public void IndexKnownAvailableShares()
        {
            IndexShares( KnownAvailableShares );
        }

        private void SaveShareIndex( IEnumerable<FileInfo> shareIndex )
        {
            using ( Manager dbman = new Manager() )
            {
                if ( shareIndex.Count() > 0 )
                {
                    FileInfo file = shareIndex.First();

                    int lastIndex = 0;
                    for ( int i = 0; i < 3; ++i )
                    {
                        lastIndex = file.FullName.IndexOf( '\\', lastIndex + 1 );
                    }

                    string shareName = file.FullName.Substring( 0, lastIndex );
                    if ( KnownShares.Contains( shareName ) )
                    {
                        dbman.SaveKnownShareStructure( shareName, shareIndex );
                    }
                    else
                    {
                        dbman.SaveShareStructure( shareName, shareIndex );
                    }
                }
            }
        }

        public static IEnumerable<FileInfo> IndexFolder( string directoryName )
        {
            List<FileInfo> listing = new List<FileInfo>();

            try
            {
                foreach ( string directory in Directory.GetDirectories( directoryName ) )
                {
                    listing.AddRange( IndexFolder( directory ) );
                }

                foreach ( string fileName in Directory.GetFiles( directoryName ) )
                {
                    listing.Add( new FileInfo( fileName ) );
                }
            }
            catch ( UnauthorizedAccessException ex )
            {
                Console.Error.WriteLine( "Access denied to {0}", directoryName );
            }
            catch ( IOException ex )
            {
                Console.Error.WriteLine( "Cannot read {0}: {1}", directoryName, ex.Message );
            }

            return listing;
        }

        #region Event handlers

        private static void OnShareDiscoveryProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            Console.Write( "\rIndexing network computers: {0}%", e.ProgressPercentage );
        }

        private static void OnShareIndexProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            string progress = "\rIndexing network shares: {0}%, ".Fmt( e.ProgressPercentage );
            string infoFormat = "{{0,-{0}}}".Fmt( Console.WindowWidth - progress.Length - 1 );

            Console.Write( progress + infoFormat, e.UserState );
        }

        #endregion Event handlers
    }
}