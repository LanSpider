﻿namespace LanSpider.Helpers
{
    /// <summary>
    /// Yields ASCII animation frames.
    /// </summary>
    public class Animator
    {
        protected string[] _frames = new[] { "\\", "|", "/", "-" };
        private int _frameNo = 0;

        public string Next()
        {
            if ( ++_frameNo == _frames.Length )
            {
                _frameNo = 0;
            }

            return _frames[ _frameNo ];
        }
    }
}