﻿using System;
using System.Collections.Generic;
using System.Data;

using System.IO;

using Extensions;

using LanSpider.Properties;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Configuration;

namespace LanSpider.Database
{
    public class Manager : IDisposable
    {
        private readonly SqlConnection _conn;

        private readonly Epam_IndexDataContext _context;

        private readonly int _rootId = 1;

        public int RootId
        {
            get
            {
                return _rootId;
            }
        }

        #region Static schema methods

        /*private static bool SchemaExists( SQLiteConnection connection )
        {
            SQLiteCommand cmd = new SQLiteCommand( null, connection );
            cmd.CommandText = "SELECT name FROM sqlite_master WHERE name=@table;";
            cmd.Parameters.Add( new SQLiteParameter( "@table", "node" ) );
            object tableName = cmd.ExecuteScalar();

            return tableName != null && tableName != DBNull.Value;
        }

        private static void CreateSchema( SQLiteConnection connection )
        {
            SQLiteCommand cmd = new SQLiteCommand( null, connection );

            cmd.CommandText = Resources.create_db;
            cmd.ExecuteNonQuery();
        }
        */

        #endregion Static schema methods

        public Manager()
        {
            _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString);
            _conn.Open();
            //_context = new Epam_IndexDataContext(ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString);
            // TO DO:

            /*if ( !SchemaExists( _conn ) )
            {
                CreateSchema( _conn );
            }*/
        }

        public void Dispose()
        {
            _conn.Close();
        }

        public List<string> GetRootFolders()
        {
            List<string> roots = new List<string>();

            using (SqlCommand cmd = new SqlCommand("SELECT path FROM folder WHERE parent_id = @root_id AND parent_id != folder_id", _conn))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("root_id", _rootId));

                using (SqlDataReader results = cmd.ExecuteReader())
                {
                    while (results.Read())
                    {
                        string x = (string)results.GetValue(0);
                        roots.Add(x);
                    }
                }
            }

            return roots;
        }

        private void IndexFile(int shareId, FileInfo file)
        {
            IndexFile(shareId, file, null);
        }

        private void IndexFile(int shareId, FileInfo file, DataTable dt)
        {
            DataRow row = dt.NewRow();
            row["parent_folder_id"] = shareId;
            row["name"] = file.Name;
            row["type"] = file.Extension;
            try
            {
                row["size"] = file.Length;
            }
            catch (FileNotFoundException)
            {
                row["size"] = -1;
            }
            dt.Rows.Add(row);
            /*using ( SqlCommand cmd = new SqlCommand(@"INSERT INTO ""file"" (parent_folder_id, name, type, size) VALUES (@parent_folder_id, @name, @type, @size)", _conn ) )
            {
                if ( trans != null )
                {
                    cmd.Transaction = trans;
                }

                cmd.CommandType = CommandType.Text;                
                cmd.Parameters.Add( new SqlParameter( "parent_folder_id", shareId ) );
                cmd.Parameters.Add(new SqlParameter("name", file.Name));
                cmd.Parameters.Add(new SqlParameter("type", file.Extension));

                try
                {
                    cmd.Parameters.Add(new SqlParameter("size", file.Length));
                }
                catch ( FileNotFoundException )
                {
                    cmd.Parameters.Add(new SqlParameter("size", -1));
                }

                cmd.ExecuteNonQuery();
            }*/
        }

        private int GetShareId(string share)
        {
            return GetShareId(share, null);
        }

        private int GetShareId(string share, SqlTransaction trans)
        {
            int shareId = 0;
            using (SqlCommand cmd = new SqlCommand("SELECT folder_id FROM folder WHERE path=@path;", _conn))
            {
                if (trans != null)
                {
                    cmd.Transaction = trans;
                }

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("path", share));

                object val = cmd.ExecuteScalar();
                if (val == null || val == DBNull.Value)
                {
                    int parentId;

                    if (share.Count('\\') == 3) // top level share
                    {
                        parentId = RootId;
                    }
                    else
                    {
                        int index = share.LastIndexOf('\\');
                        string parentName = share.Substring(0, index);
                        parentId = GetShareId(parentName, trans);
                    }
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "AddFolder";
                    cmd.Parameters.Add(new SqlParameter("parent_id", parentId));
                    cmd.Parameters.Add(new SqlParameter("folder_id", SqlDbType.Int)).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    shareId = (int)cmd.Parameters["folder_id"].Value;
                }
                else
                    return (int)val;
            }

            return shareId;
        }

        public void SaveShareStructure(string shareName, IEnumerable<FileInfo> fileInfos)
        {
            SqlTransaction trans = _conn.BeginTransaction();

            try
            {
                SaveShareStructure(shareName, fileInfos, trans);
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                Trace.WriteLine("Share " + shareName + " will not be saved: " + ex.Message);
                Console.Error.WriteLine("Share " + shareName + " will not be saved: " + ex.Message);
                //throw;
            }
        }

        public void SaveShareStructure(string shareName, IEnumerable<FileInfo> fileInfos, SqlTransaction trans)
        {
            using (DataTable dt = new DataTable("[File]"))
            {
                SqlBulkCopy bulk = new SqlBulkCopy(_conn, SqlBulkCopyOptions.Default, trans);
                bulk.DestinationTableName = dt.TableName;
                bulk.BatchSize = 1000;
                dt.Columns.Add(new DataColumn("file_id", typeof(int)));
                dt.Columns.Add(new DataColumn("parent_folder_id", typeof(int)));
                dt.Columns.Add(new DataColumn("name", typeof(string)));
                dt.Columns.Add(new DataColumn("type", typeof(string)));
                dt.Columns.Add(new DataColumn("size", typeof(long)));

                foreach (FileInfo fileInfo in fileInfos)
                {
                    // TO DO: GetShare as SP
                    int shareId = GetShareId(fileInfo.DirectoryName, trans);
                    IndexFile(shareId, fileInfo, dt);
                }
                bulk.WriteToServer(dt);
                dt.Clear();
                GC.Collect();
            }
        }

        public void SaveKnownShareStructure(string shareName, IEnumerable<FileInfo> fileInfos)
        {
            SqlTransaction trans = _conn.BeginTransaction();

            try
            {
                using (var cmd = new SqlCommand(@"DELETE FROM [FILE] WHERE parent_folder_id in (SELECT folder_id FROM Folder WHERE path like @share +'\%' or path = @share)", _conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Transaction = trans;
                    cmd.CommandTimeout = 0;
                    cmd.Parameters.Add(new SqlParameter("share", shareName));
                    cmd.ExecuteNonQuery();                    
                    //cmd.CommandText = @"DELETE FROM FOLDER WHERE folder_id in (select folder_id from FOLDER WHERE PATH like @share + '\%')";
                    //cmd.ExecuteNonQuery();
                }

                SaveShareStructure(shareName, fileInfos, trans);
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                Trace.WriteLine("Share " + shareName + " will not be saved: " + ex.Message);
                Console.Error.WriteLine("Share " + shareName + " will not be saved: " + ex.Message);
                //throw;
            }
        }
    }
}