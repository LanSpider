CREATE TABLE  "folder" (
    [folder_id] INTEGER  NOT NULL PRIMARY KEY IDENTITY(1,1),
    [parent_id] INTEGER,
    [path] NVARCHAR(266)  UNIQUE NOT NULL,
    [indexed_on] DATETIME DEFAULT GETDATE() NOT NULL,

    CONSTRAINT FK_FOLDER_PARENT_FOLDER_ID_TO_FOLDER_FOLDER_ID
	    FOREIGN KEY (parent_id) REFERENCES folder(folder_id) ON DELETE NO ACTION,
	    
    CONSTRAINT IDX_FOLDER_PATH
	    UNIQUE (path)
);

CREATE TABLE "file" (
    [file_id] INTEGER  NOT NULL PRIMARY KEY IDENTITY(1,1),
    [parent_folder_id] INTEGER  NOT NULL,
    [name] NVARCHAR(255),
    [type] VARCHAR(250),
    [size] BIGINT NOT NULL,
    [indexed_on] DATETIME DEFAULT GETDATE() NOT NULL,

    CONSTRAINT FK_FILE_PARENT_FOLDER_ID_TO_FOLDER_FOLDER_ID
        FOREIGN KEY (parent_folder_id) REFERENCES folder(folder_id) ON DELETE CASCADE,
    
    CONSTRAINT IDX_NAME_PARENT_FOLDER_ID
	    UNIQUE (parent_folder_id, name)
);

SET IDENTITY_INSERT FOLDER ON;

INSERT INTO folder (folder_id, parent_id, path)
        VALUES (1, null, 'root');