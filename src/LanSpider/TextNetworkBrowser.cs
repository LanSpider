﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LanSpider
{
    /// <summary>
    /// Fake network browser which takes information from file.
    /// </summary>
    internal class TextNetworkBrowser : INetworkBrowser
    {
        private readonly string _filename;

        public TextNetworkBrowser( string filename )
        {
            _filename = filename;
        }

        public IEnumerable<string> GetNetworkComputers()
        {
            return ReadItems( 2 );
        }

        public IEnumerable<string> GetShares( string serverName )
        {
            return ReadItems( serverName, 3, true );
        }

        public IEnumerable<FileInfo> GetSharedFiles( string shareName )
        {
            return from filename in ReadItems( shareName, true )
                   select new FileInfo( filename );
        }

        #region Overloads

        private IEnumerable<string> ReadItems( string mask, int clipDepth )
        {
            return ReadItems( mask, clipDepth, false );
        }

        private IEnumerable<string> ReadItems( int clipDepth )
        {
            return ReadItems( clipDepth, false );
        }

        private IEnumerable<string> ReadItems( int clipDepth, bool entriesGroupped )
        {
            return ReadItems( null, clipDepth, entriesGroupped );
        }

        private IEnumerable<string> ReadItems( string mask, bool entriesGroupped )
        {
            return ReadItems( mask, 0, entriesGroupped );
        }

        #endregion Overloads

        private IEnumerable<string> ReadItems( string mask, int clipDepth, bool entriesGroupped )
        {
            List<string> items = new List<string>();

            using ( StreamReader reader = File.OpenText( _filename ) )
            {
                string line;

                while ( !( String.IsNullOrEmpty( line = reader.ReadLine() ) ) )
                {
                    string path = line.Trim( new[] { '[', ']', ' ', '\t' } );
                    if ( String.IsNullOrEmpty( mask ) || path.StartsWith( mask ) )
                    {
                        string item;

                        if ( clipDepth > 0 )
                        {
                            int lastIndex = 0;
                            for ( int i = 0; i < clipDepth; ++i )
                            {
                                lastIndex = path.IndexOf( '\\', lastIndex + 1 );
                            }

                            item = path.Substring( 0, lastIndex );
                        }
                        else
                        {
                            item = path;
                        }

                        if ( !items.Contains( item ) )
                        {
                            items.Add( item );
                        }
                    }
                    else if ( entriesGroupped && items.Count > 0 )
                    {
                        break;
                    }
                }
            }

            return items;
        }
    }
}