﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Windows.Forms;

using Extensions;

namespace LanSpider
{
    /// <summary>
    /// Gets a list of machines and open shares on LAN. Uses DllImport of Netapi32.dll.
    /// </summary>
    public sealed class NetworkBrowser : INetworkBrowser
    {
        private static NetworkBrowser _instance;

        private IEnumerable<string> _windowsNetworks;

        private NetworkBrowser()
        {
        }

        public static NetworkBrowser Instance
        {
            get
            {
                if ( _instance == null )
                {
                    _instance = new NetworkBrowser();
                }
                return _instance;
            }
        }

        #region Dll Imports

        /// <summary>
        /// Netapi32.dll : Get list of servers on the network.
        /// </summary>
        [DllImport( "Netapi32", CharSet = CharSet.Auto, SetLastError = true ), SuppressUnmanagedCodeSecurity]
        private static extern int NetServerEnum(
            string ServerNane, // must be null
            int dwLevel,
            ref IntPtr pBuf,
            int dwPrefMaxLen,
            out int dwEntriesRead,
            out int dwTotalEntries,
            int dwServerType,
            string domain, // null for login domain
            out int dwResumeHandle
            );

        /// <summary>
        /// Netapi32.dll : Get list of shares on given machine.
        /// </summary>
        /// <returns></returns>
        [DllImport( "Netapi32", CharSet = CharSet.Auto, SetLastError = true ), SuppressUnmanagedCodeSecurity]
        private static extern int NetShareEnum(
            string servername,
            int level,
            ref IntPtr bufptr,
            int prefmaxlen,
            out int entriesread,
            out int totalentries,
            out int resume_handle
            );


        /// <summary>
        /// Netapi32.dll : The NetApiBufferFree function frees the memory 
        /// that the NetApiBufferAllocate function allocates. 
        /// Call NetApiBufferFree to free the memory that other network management functions return.
        /// </summary>
        [DllImport( "Netapi32", SetLastError = true ), SuppressUnmanagedCodeSecurity]
        private static extern int NetApiBufferFree(
            IntPtr pBuf );

        /// <summary>
        /// ServerInfo structure holds information on network servers returned by <see cref="NetServerEnum"/>.
        /// </summary>
        [StructLayout( LayoutKind.Sequential )]
        private struct _SERVER_INFO_100
        {
            internal int sv100_platform_id;
            [MarshalAs( UnmanagedType.LPWStr )] internal string sv100_name;
        }

        /// <summary>
        /// ShareInfo1 structure hold share infomation as returned by <see cref="NetShareEnum"/> level 1.
        /// </summary>
        [StructLayout( LayoutKind.Sequential )]
        private struct _SHARE_INFO_1
        {
            [MarshalAs( UnmanagedType.LPWStr )] internal string shi1_netname;

            internal uint shi1_type;
            [MarshalAs( UnmanagedType.LPWStr )] internal string shi1_remark;
        }

        [DllImport( "Mpr.dll", EntryPoint = "WNetOpenEnumA", CallingConvention = CallingConvention.Winapi )]
        private static extern ErrorCodes WNetOpenEnum( ResourceScope dwScope, ResourceType dwType, ResourceUsage dwUsage,
                                                       NETRESOURCE p, out IntPtr lphEnum );

        [DllImport( "Mpr.dll", EntryPoint = "WNetCloseEnum", CallingConvention = CallingConvention.Winapi )]
        private static extern ErrorCodes WNetCloseEnum( IntPtr hEnum );

        [DllImport( "Mpr.dll", EntryPoint = "WNetEnumResourceA", CallingConvention = CallingConvention.Winapi )]
        private static extern ErrorCodes WNetEnumResource( IntPtr hEnum, ref uint lpcCount, IntPtr buffer,
                                                           ref uint lpBufferSize );

        #endregion

        #region Constants

        private const int MAX_PREFERRED_LENGTH = -1;

        private const int SV_TYPE_WORKSTATION = 1;
        private const int SV_TYPE_SERVER = 2;

        private const uint STYPE_PRINTQ = 1;
        private const uint STYPE_DEVICE = 2;
        private const uint STYPE_IPC = 3;

        private const uint STYPE_SPECIAL = 0x80000000;
        private const uint STYPE_TEMPORARY = 0x40000000;

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets Microsoft Windows Networks.
        /// </summary>
        public IEnumerable<string> WindowsNetworks
        {
            get
            {
                if ( _windowsNetworks == null )
                {
                    NETRESOURCE netRoot = new NETRESOURCE();
                    IEnumerable<string> networkObjects
                        = EnumerateServers( netRoot, ResourceScope.RESOURCE_GLOBALNET, ResourceType.RESOURCETYPE_DISK,
                                            ResourceUsage.RESOURCEUSAGE_ALL,
                                            ResourceDisplayType.RESOURCEDISPLAYTYPE_NETWORK, "" );

                    _windowsNetworks = from nwo in networkObjects
                                       where nwo.EndsWith( "|RESOURCEDISPLAYTYPE_DOMAIN" )
                                       select nwo.Remove( nwo.LastIndexOf( '|' ) );
                }

                return _windowsNetworks;
            }
        }

        /// <summary>
        /// Get public shares defined in network given.
        /// </summary>
        /// <param name="networkName"></param>
        /// <returns></returns>
        public IEnumerable<string> GetNetworkShares( string networkName )
        {
            IEnumerable<string> shares = null;

            if ( WindowsNetworks.Contains( networkName ) )
            {
                NETRESOURCE netRoot = new NETRESOURCE();
                IEnumerable<string> machines
                    = EnumerateServers( netRoot, ResourceScope.RESOURCE_GLOBALNET, ResourceType.RESOURCETYPE_DISK,
                                        ResourceUsage.RESOURCEUSAGE_ALL, ResourceDisplayType.RESOURCEDISPLAYTYPE_SERVER,
                                        networkName );

                shares = from share in machines
                         where share.Count( '\\' ) >= 3
                         select share;
            }

            return shares;
        }


        /// <summary>
        /// List network ResourceScope using Mpr.dll functions.
        /// </summary>
        /// <param name="pRsrc"></param>
        /// <param name="scope"></param>
        /// <param name="type"></param>
        /// <param name="usage"></param>
        /// <param name="displayType"></param>
        /// <param name="kPath"></param>
        /// <returns></returns>
        private static IEnumerable<string> EnumerateServers( NETRESOURCE pRsrc, ResourceScope scope, ResourceType type,
                                                             ResourceUsage usage, ResourceDisplayType displayType,
                                                             string kPath )
        {
            List<string> aData = new List<string>();

            uint bufferSize = 16384;
            IntPtr buffer = Marshal.AllocHGlobal( (int) bufferSize );
            IntPtr handle;
            uint cEntries = 1;
            bool serverenum = false;

            ErrorCodes result = WNetOpenEnum( scope, type, usage, pRsrc, out handle );

            if ( result == ErrorCodes.NO_ERROR )
            {
                do
                {
                    result = WNetEnumResource( handle, ref cEntries, buffer, ref bufferSize );

                    if ( ( result == ErrorCodes.NO_ERROR ) )
                    {
                        Marshal.PtrToStructure( buffer, pRsrc );

                        if ( String.Compare( kPath, "" ) == 0 )
                        {
                            if ( ( pRsrc.dwDisplayType == displayType ) ||
                                 ( pRsrc.dwDisplayType == ResourceDisplayType.RESOURCEDISPLAYTYPE_DOMAIN ) )
                            {
                                aData.Add( pRsrc.lpRemoteName + "|" + pRsrc.dwDisplayType );
                            }

                            if ( ( pRsrc.dwUsage & ResourceUsage.RESOURCEUSAGE_CONTAINER ) ==
                                 ResourceUsage.RESOURCEUSAGE_CONTAINER )
                            {
                                if ( ( pRsrc.dwDisplayType == displayType ) )
                                {
                                    aData.AddRange( EnumerateServers( pRsrc, scope, type, usage, displayType, kPath ) );
                                }
                            }
                        }
                        else
                        {
                            if ( pRsrc.dwDisplayType == displayType )
                            {
                                aData.Add( pRsrc.lpRemoteName );
                                aData.AddRange( EnumerateServers( pRsrc, scope, type, usage, displayType, kPath ) );
                                //return;
                                serverenum = true;
                            }
                            if ( !serverenum )
                            {
                                if ( pRsrc.dwDisplayType == ResourceDisplayType.RESOURCEDISPLAYTYPE_SHARE )
                                {
                                    aData.Add( pRsrc.lpRemoteName + "-share" );
                                }
                            }
                            else
                            {
                                serverenum = false;
                            }
                            if ( ( kPath.IndexOf( pRsrc.lpRemoteName ) >= 0 ) ||
                                 ( String.Compare( pRsrc.lpRemoteName, "Microsoft Windows Network" ) == 0 ) )
                            {
                                aData.AddRange( EnumerateServers( pRsrc, scope, type, usage, displayType, kPath ) );
                            }
                        }
                    }
                    else if ( result != ErrorCodes.ERROR_NO_MORE_ITEMS )
                    {
                        break;
                    }
                }
                while ( result != ErrorCodes.ERROR_NO_MORE_ITEMS );

                WNetCloseEnum( handle );
            }

            Marshal.FreeHGlobal( buffer );

            return aData;
        }


        public enum ResourceScope
        {
            RESOURCE_CONNECTED = 1,
            RESOURCE_GLOBALNET,
            RESOURCE_REMEMBERED,
            RESOURCE_RECENT,
            RESOURCE_CONTEXT
        } ;

        public enum ResourceType
        {
            RESOURCETYPE_ANY,
            RESOURCETYPE_DISK,
            RESOURCETYPE_PRINT,
            RESOURCETYPE_RESERVED
        } ;

        [Flags]
        public enum ResourceUsage
        {
            RESOURCEUSAGE_CONNECTABLE = 0x00000001,
            RESOURCEUSAGE_CONTAINER = 0x00000002,
            RESOURCEUSAGE_NOLOCALDEVICE = 0x00000004,
            RESOURCEUSAGE_SIBLING = 0x00000008,
            RESOURCEUSAGE_ATTACHED = 0x00000010,
            RESOURCEUSAGE_ALL = ( RESOURCEUSAGE_CONNECTABLE | RESOURCEUSAGE_CONTAINER | RESOURCEUSAGE_ATTACHED ),
        } ;

        internal enum ErrorCodes
        {
            NO_ERROR = 0,
            ERROR_NO_MORE_ITEMS = 259
        } ;

        public enum ResourceDisplayType
        {
            RESOURCEDISPLAYTYPE_GENERIC,
            RESOURCEDISPLAYTYPE_DOMAIN,
            RESOURCEDISPLAYTYPE_SERVER,
            RESOURCEDISPLAYTYPE_SHARE,
            RESOURCEDISPLAYTYPE_FILE,
            RESOURCEDISPLAYTYPE_GROUP,
            RESOURCEDISPLAYTYPE_NETWORK,
            RESOURCEDISPLAYTYPE_ROOT,
            RESOURCEDISPLAYTYPE_SHAREADMIN,
            RESOURCEDISPLAYTYPE_DIRECTORY,
            RESOURCEDISPLAYTYPE_TREE,
            RESOURCEDISPLAYTYPE_NDSCONTAINER
        } ;

        [StructLayout( LayoutKind.Sequential )]
        internal class NETRESOURCE
        {
            public ResourceScope dwScope = 0;
            public ResourceType dwType = 0;
            public ResourceDisplayType dwDisplayType = 0;
            public ResourceUsage dwUsage = 0;
            public string lpLocalName;
            public string lpRemoteName;
            public string lpComment;
            public string lpProvider;

            public NETRESOURCE()
            {
                lpProvider = null;
            }
        } ;

        /// <summary>
        /// Get list of share names available on given server.
        /// </summary>
        /// <param name="serverName">Network machine name.</param>
        /// <returns>List of shares on server.</returns>
        public IEnumerable<string> GetShares( string serverName )
        {
            IntPtr buffer = IntPtr.Zero;

            List<string> shares = new List<string>();
            int sizeofInfo = Marshal.SizeOf( typeof( _SHARE_INFO_1 ) );

            try
            {
                int entriesRead;
                int totalEntries;
                int resHandle;
                int ret = NetShareEnum( serverName, 1, ref buffer, MAX_PREFERRED_LENGTH, out entriesRead,
                                        out totalEntries,
                                        out resHandle );

                if ( ret == 0 )
                {
                    for ( int i = 0; i < totalEntries; i++ )
                    {
                        // Get next pointer to share info structure
                        IntPtr tmpBuffer = new IntPtr( (int) buffer + ( i * sizeofInfo ) );

                        // Get structure from pointer
                        _SHARE_INFO_1 shareInfo =
                            (_SHARE_INFO_1) Marshal.PtrToStructure( tmpBuffer, typeof( _SHARE_INFO_1 ) );

                        // Skipe special types of shares
                        if ( ( shareInfo.shi1_type & STYPE_TEMPORARY ) == STYPE_TEMPORARY
                             || ( shareInfo.shi1_type & STYPE_IPC ) == STYPE_IPC
                             || ( shareInfo.shi1_type & STYPE_DEVICE ) == STYPE_DEVICE
                             || ( shareInfo.shi1_type & STYPE_PRINTQ ) == STYPE_PRINTQ
                             || ( shareInfo.shi1_type & STYPE_SPECIAL ) == STYPE_SPECIAL
                             || shareInfo.shi1_remark == "Printer Drivers" )
                        {
                            continue;
                        }

                        shares.Add( @"\\" + serverName + @"\" + shareInfo.shi1_netname );
                    }
                }
            }
            catch ( Exception ex )
            {
                MessageBox.Show( "Problem with acessing " +
                                 "remote shares in NetworkBrowser " +
                                 "\r\n\r\n\r\n" + ex.Message,
                                 "Error", MessageBoxButtons.OK,
                                 MessageBoxIcon.Error );
                return null;
            }
            finally
            {
                // Free allocated memory
                NetApiBufferFree( buffer );
            }

            return shares;
        }

        /// <summary>
        /// Get the list of network machine of types serves and workstation.
        /// </summary>
        /// <returns>List of machines in network.</returns>
        public IEnumerable<string> GetNetworkComputers()
        {
            IntPtr buffer = IntPtr.Zero;
            int sizeofInfo = Marshal.SizeOf( typeof( _SERVER_INFO_100 ) );
            string[] networkComputers = null;
            try
            {
                // Get list of machines
                int entriesRead;
                int totalEntries;
                int resHandle;
                int ret = NetServerEnum( null, 100, ref buffer, MAX_PREFERRED_LENGTH, out entriesRead, out totalEntries,
                                         SV_TYPE_WORKSTATION | SV_TYPE_SERVER, null, out resHandle );

                if ( ret == 0 )
                {
                    networkComputers = new string[totalEntries];
                    for ( int i = 0; i < totalEntries; i++ )
                    {
                        // Get next pointer to server info structure
                        IntPtr tmpBuffer = new IntPtr( (int) buffer + ( i * sizeofInfo ) );

                        // Get structure from pointer
                        _SERVER_INFO_100 svrInfo =
                            (_SERVER_INFO_100) Marshal.PtrToStructure( tmpBuffer, typeof( _SERVER_INFO_100 ) );

                        networkComputers[ i ] = svrInfo.sv100_name;
                    }
                }
            }
            catch ( Exception ex )
            {
                MessageBox.Show( "Problem with acessing " +
                                 "network computers in NetworkBrowser " +
                                 "\r\n\r\n\r\n" + ex.Message,
                                 "Error", MessageBoxButtons.OK,
                                 MessageBoxIcon.Error );
                return null;
            }
            finally
            {
                // Free allocated memory
                NetApiBufferFree( buffer );
            }

            return networkComputers;
        }

        #endregion
    }
}