﻿using System;
using System.Collections.Generic;

namespace LanSpider.Multithreading
{
    /// <summary>
    /// Consumes work items produced by WorkManager.
    /// </summary>
    public class Consumer< TResult >
    {
        private List<TResult> _results;
        private readonly object _resultsLock = new object();

        public IEnumerable<TResult> Results
        {
            get
            {
                if ( _results == null )
                {
                    throw new InvalidOperationException( "Cannot return results before work was preformed" );
                }

                return _results;
            }
        }

        public void Consume( TResult result )
        {
            lock ( _resultsLock )
            {
                if ( _results == null )
                {
                    _results = new List<TResult>();
                }

                _results.Add( result );
            }
        }
    }
}