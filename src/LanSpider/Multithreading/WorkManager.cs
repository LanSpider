﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace LanSpider.Multithreading
{
    /// <summary>
    /// Manages concurent execution of several threads of worker method and 
    /// pipes results into consumer method.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    public class WorkManager< T, TResult >
    {
        private readonly Func<T, TResult> _workerMethod;
        private readonly Action<TResult> _consumerMethod;
        private Queue<T> _input;

        private readonly object _inputHandle = new object();
        private readonly object _outputHandle = new object();

        public event EventHandler<ProgressChangedEventArgs> WorkProgressChanged;
        private int _totalItems;

        /// <summary>
        /// Initialize an istance of <see cref="WorkManager{T,TResult}"/> class with
        /// worker and consumer methods.
        /// </summary>
        /// <param name="workerMethod"></param>
        /// <param name="consumerMethod"></param>
        public WorkManager( Func<T, TResult> workerMethod, Action<TResult> consumerMethod )
        {
            _workerMethod = workerMethod;
            _consumerMethod = consumerMethod;
        }

        /// <summary>
        /// Execute concurent worker on data provided and return in a synchronous manner.
        /// </summary>
        /// <param name="shares"></param>
        public void RunSync( IEnumerable<T> shares )
        {
            _input = new Queue<T>( shares );
            _totalItems = _input.Count;

            Action yield = Yield;

            int nThreads = Environment.ProcessorCount * 2;

            List<IAsyncResult> handles = new List<IAsyncResult>( nThreads );

            for ( int i = 0; i < nThreads; ++i )
            {
                IAsyncResult iar = yield.BeginInvoke( null, null );
                handles.Add( iar );
            }

            foreach ( IAsyncResult asyncResult in handles )
            {
                asyncResult.AsyncWaitHandle.WaitOne();
            }
        }

        private void Yield()
        {
            while ( true )
            {
                T argument = default( T );
                bool gotWork = false;

                lock ( _inputHandle )
                {
                    if ( _input.Count > 0 )
                    {
                        argument = _input.Dequeue();
                        gotWork = true;
                    }
                }

                if ( !gotWork )
                {
                    break;
                }

                TResult result = _workerMethod( argument );

                if ( WorkProgressChanged != null )
                {
                    lock ( _inputHandle )
                    {
                        WorkProgressChanged( this,new ProgressChangedEventArgs( 100 - _input.Count * 100 / _totalItems, argument ) );
                    }
                }

                // TO DO: lock ( _outputHandle )
                {
                    _consumerMethod( result );
                }
            }
        }

    }
}