using System.Collections.Generic;

namespace LanSpider
{
    public interface INetworkBrowser
    {
        /// <summary>
        /// Get list of share names available on given server.
        /// </summary>
        /// <param name="serverName">Network machine name.</param>
        /// <returns>List of shares on server.</returns>
        IEnumerable<string> GetShares( string serverName );

        /// <summary>
        /// Get the list of network machine of types serves and workstation.
        /// </summary>
        /// <returns>List of machines in network.</returns>
        IEnumerable<string> GetNetworkComputers();
    }
}