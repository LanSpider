﻿<%@ Page Title="LanSpider Web Search" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <h1>LanSpider Web Search</h1>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    <asp:Button ID="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click" PostBackUrl=""
        />
    <br />
    <asp:RadioButtonList ID="rblOptions" runat="server" 
        RepeatDirection="Horizontal">
        <asp:ListItem Selected="True" Value="0">All</asp:ListItem>
        <asp:ListItem Value="1">Images</asp:ListItem>
        <asp:ListItem Value="2">Video</asp:ListItem>
        <asp:ListItem Value="3">Music</asp:ListItem>
    </asp:RadioButtonList>
    <asp:ListView ID="ListView1" runat="server">
        <ItemTemplate>
            <tr style="">
                <td>
                    <asp:Label ID="nameLabel" runat="server" Text='<%# Eval("name") %>' />
                </td>
                <td>
                    <a runat="server" id="pathLink" href='<%# Eval("path") %>' ><%# Eval("path") %></a>
                </td>
                <td>
                    <asp:Label ID="sizeLabel" runat="server" Text='<%# Eval("size") %>' />
                </td>
                <td>
                    <asp:Label ID="typeLabel" runat="server" Text='<%# Eval("type") %>' />
                </td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr style="">
                <td>
                    <asp:Label ID="nameLabel" runat="server" Text='<%# Eval("name") %>' />
                </td>
                <td>
                <a runat="server" id="pathLink" href='<%# Eval("path") %>' ><%# Eval("path") %></a>
                </td>
                <td>
                    <asp:Label ID="sizeLabel" runat="server" Text='<%# Eval("size") %>' />
                </td>
                <td>
                    <asp:Label ID="typeLabel" runat="server" Text='<%# Eval("type") %>' />
                </td>
            </tr>
        </AlternatingItemTemplate>
        <EmptyDataTemplate>
            <table runat="server" style="">
                <tr>
                    <td>
                        No files was found.</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <InsertItemTemplate>
            <tr style="">
                <td>
                    <asp:Button ID="InsertButton" runat="server" CommandName="Insert" 
                        Text="Insert" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" 
                        Text="Clear" />
                </td>
                <td>
                    <asp:TextBox ID="nameTextBox" runat="server" Text='<%# Bind("name") %>' />
                </td>
                <td>
                    <asp:TextBox ID="pathTextBox" runat="server" Text='<%# Bind("path") %>' />
                </td>
                <td>
                    <asp:TextBox ID="sizeTextBox" runat="server" Text='<%# Bind("size") %>' />
                </td>
                <td>
                    <asp:TextBox ID="typeTextBox" runat="server" Text='<%# Bind("type") %>' />
                </td>
            </tr>
        </InsertItemTemplate>
        <LayoutTemplate>
            <table runat="server">
                <tr runat="server">
                    <td runat="server">
                        <table ID="itemPlaceholderContainer" runat="server" border="0" style="">
                            <tr runat="server" style="">
                                <th runat="server">
                                    File name</th>
                                <th runat="server">
                                    Path</th>
                                <th runat="server">
                                    Size</th>
                                <th runat="server">
                                    Type</th>
                            </tr>
                            <tr ID="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr runat="server">
                    <td runat="server" style="">
                        <asp:DataPager ID="DataPager1" runat="server">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" 
                                    ShowNextPageButton="False" ShowPreviousPageButton="False" />
                                <asp:NumericPagerField />
                                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" 
                                    ShowNextPageButton="False" ShowPreviousPageButton="False" />
                            </Fields>
                        </asp:DataPager>
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <EditItemTemplate>
            <tr style="">
                <td>
                    <asp:Button ID="UpdateButton" runat="server" CommandName="Update" 
                        Text="Update" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" 
                        Text="Cancel" />
                </td>
                <td>
                    <asp:TextBox ID="nameTextBox" runat="server" Text='<%# Bind("name") %>' />
                </td>
                <td>
                    <asp:TextBox ID="pathTextBox" runat="server" Text='<%# Bind("path") %>' />
                </td>
                <td>
                    <asp:TextBox ID="sizeTextBox" runat="server" Text='<%# Bind("size") %>' />
                </td>
                <td>
                    <asp:TextBox ID="typeTextBox" runat="server" Text='<%# Bind("type") %>' />
                </td>
            </tr>
        </EditItemTemplate>
        <SelectedItemTemplate>
            <tr style="">
                <td>
                    <asp:Label ID="nameLabel" runat="server" Text='<%# Eval("name") %>' />
                </td>
                <td>
                    <asp:Label ID="pathLabel" runat="server" Text='<%# Eval("path") %>' />
                </td>
                <td>
                    <asp:Label ID="sizeLabel" runat="server" Text='<%# Eval("size") %>' />
                </td>
                <td>
                    <asp:Label ID="typeLabel" runat="server" Text='<%# Eval("type") %>' />
                </td>
            </tr>
        </SelectedItemTemplate>
    </asp:ListView>
    <asp:LinqDataSource ID="dsLinq" runat="server" 
        ContextTypeName="SearchIndexDataContext" Select="new (name, path, size, type)" 
        TableName="FileInfos">
    </asp:LinqDataSource>
    <br />
</asp:Content>

