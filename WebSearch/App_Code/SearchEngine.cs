﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

/// <summary>
/// Summary description for SearchEngine
/// </summary>
public class SearchEngine
{
    private SearchIndexDataContext _dataContext = new SearchIndexDataContext(ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString);
    public SearchEngine()
    {        
    }

    public System.Data.Linq.ISingleResult<FileInfo> Search(string phrase)
    {
        var result  = _dataContext.Search(phrase, "Any");
        return result;
    }
}
