﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string searchMask = Request.QueryString["q"];
        if (!String.IsNullOrEmpty(searchMask))
        {
            Search(searchMask);
        }
        else
        {
            ListView1.DataSource = null;
            ListView1.DataBind();
        }
    }

    protected void Search(string searchMask)
    {
        SearchEngine se = new SearchEngine();
        ListView1.DataSource = se.Search(searchMask);
        ListView1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if  (!String.IsNullOrEmpty(this.TextBox1.Text))
            Response.Redirect(String.Format("~/Default.aspx?q={0}", this.TextBox1.Text));
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (this.IsPostBack && (Request.Params["__EVENTTARGET"] == btnSearch.UniqueID))
            Response.Redirect("~/Default.aspx?q=" + this.Server.UrlEncode(Request.Params[TextBox1.UniqueID]));
    }
}
